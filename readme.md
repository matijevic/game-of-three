# Game of Three

### thank you for the coding challenge!

I had a lot of fun with it :)

### how to run the project

First clone the repo: `git clone https://matijevic@bitbucket.org/matijevic/game-of-three.git`

`cd` into the new directory: `cd game-of-three`

Then install all dependencies: `npm install`

Then run the project: `npm run dev`

### Tech-stack

This project is built with Next.js and socket.io.

I chose Next.js because of convenience. I wanted to work with React, but for the sake of time I didn't want to go through the effort of setting up webpack, babel, and a server. Next.js gives me everything I need straight out of the box.

I chose to write the API with sockets. I chose sockets because I thought it offered more interesting ways to scale the game (should I ever choose to do so) than a RESTful API would. With sockets I can relatively easily find out which players are currently online, and then allow online players to challenge each other, allow players to chat with each other, and other features that might give this simple game a more MMO-feel to it!

### Structure

Server-side I have my usual `index.js` file that contains all of my server-side socket code, as well as a `game.js` file that contains helper functions.

Client-side I have of four total components. I have my `Main` component, that renders `PreGame` and `Game`.

The `PreGame` renders the `PreGame` experience (what the user sees before the game starts), and `Game` renders the game experience.

`PreGame` renders `GameConfiguration`, which is a component that, if fully realized, would allow the user to customize the game before playing it.

`Main` is meant to be my store, in that it is responsible for managing most of the state of the game, and it passes down props to its children as they need it. I didn't think the game was a big enough project to warrant a state management library like Redux, so instead I have `Main` act as my store.

I use hooks to manage most of the client-side logic. I wrote two custom hooks - `useSockets` and `useStatefulFields`. `useSockets` handles all of the client-side socket logic. It receives events and updates state accordingly, and also emits events to the server when necessary. `useStatefulFields` is responsible for handling user input and storing it in state.

### What Went Well

The game works, and I'm happy with the minimalist styling of it.

I'm quite happy with how the API turned out. It works - it does all the logic it needs to, sends messages to the correct clients, and I think it is not super difficult to follow my logic.

I'm also quite happy with the structure of the game. I think the way I structured my components was pretty straight-forward, and extracting all the logic out to hooks makes the components themselves relatively straight-forward to follow I think. My components render things, and the hooks take care of the logic.

### Areas of Improvement

My biggest regret was not thinking more critically about all the different states the game can have. I started coding this project simply thinking there were two states: (1) pre-game state (what the user sees before the game starts); and (2) the game state (what the user sees when the game is actively being played).

One embarrassing oversight of mine is that I did not consider a third state: the post-game state!

What I did not appreciate until after I started coding is that there might be different states for different users. For example, one of the requirements of the challenge was: "One of the players should optionally be adjustable by a user." So this means that the pre-game state can be further divided into two different experiences so to speak: there's the pre-game adjustable user state, and there's the pre-game not-adjustable-user state.

The challenge I ended up running into because of this was that I decided to manage which state should be shown with booleans. This is ok if you only have two different states. But as the project grew and I realized there were more states, I started requiring more booleans, and all these booleans became a bit challenging to manage!

If I could re-do the challenge, then, I would approach the challenge keeping in mind all of the following states:

1. pre-game
    - adjustable-user
    - not-adjustable-user
2. game
    - user who is sent a number
    - other user who is awaiting their turn
3. post-game state
    - winner
    - loser
